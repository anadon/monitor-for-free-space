#include <algorithm>
#include <cstdio>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <linux/fanotify.h>
#include <sys/fanotify.h>
#include <sys/statvfs.h>
#include <string.h>
#include <unistd.h>
#include <utility>

using std::get;
using std::max;
using std::pair;

#define EXITING_WITH_ERRIR "Exiting with error"

enum class truth_class { decided_true, decided_false, undecidable };

// To consistently handle off-by-one type potential errors.
constexpr bool below_quota_comparator(const auto measured_size,
				      const auto quota) noexcept
{
	return measured_size < quota;
}

//message, is it recoverable (0) else what exit code to exit with.
constexpr pair<pair<char *, int>, bool>
statvfs_error_info(const int status) noexcept
{
	if (status == 0)
		return { { "", 0 }, false };
	return { { strerror(errno),
		   errno == EINTR || errno == EIO || errno == EOVERFLOW ?
			   0 :
			   errno },
		 true };
}

constexpr truth_class has_quota_been_exceeded(const char *mnt,
					      const size_t quota) noexcept
{
	struct statvfs mnt_pnt_stats;

	auto call_stat = statvfs_error_info(statvfs(mnt, &mnt_pnt_stats));

	if (get<1>(call_stat) != false) {
		if (get<1>(get<0>(call_stat))) {
			// Fatal Error
			error_at_line(
				get<1>(get<0>(call_stat)),
				get<1>(get<0>(call_stat)), __FILE__, __LINE__,
				"Fatal error: statvfs(7) called on '%s' returned non-zero status %d (%s); exiting with error",
				mnt, get<1>(get<0>(call_stat)),
				get<0>(get<0>(call_stat)));
		} else {
			//Non-fatal Error
			error_at_line(
				0, get<1>(get<0>(call_stat)), __FILE__,
				__LINE__,
				"Error: statvfs(7) called on '%s' returned non-zero status (%s); exiting with error",
				mnt, get<0>(get<0>(call_stat)));

			return truth_class::undecidable;
		}
	}

	// We have good data from our call, now check if we need to allocate more space.
	return below_quota_comparator(
		       mnt_pnt_stats.f_bsize * mnt_pnt_stats.f_bfree, quota) ?
		       truth_class::decided_true :
		       truth_class::decided_false;
}

// Since re-try behavior may need to be limited in some intelligent or even configurable
// in the future, set up a wrapper function to handle this logic.
constexpr bool safe_has_quota_been_exceeded(const char *mnt,
					    const size_t quota) noexcept
{
	for (int i = 0; i < 5; ++i) {
		auto quota_check = has_quota_been_exceeded(mnt, quota);
		if (quota_check != truth_class::undecidable) {
			switch (quota_check) {
			case truth_class::decided_true:
				return true;
			case truth_class::decided_false:
				return false;
			default:
				error_at_line(
					EOPNOTSUPP, EOPNOTSUPP, __FILE__,
					__LINE__,
					"Internal programming error.  Should not be possible to reach here.");
				std::unreachable();
			};
		}
	}

	error_at_line(EBUSY, EBUSY, __FILE__, __LINE__, EXITING_WITH_ERRIR);
	std::unreachable();
}

constexpr void monitor_storage_loop(const char *mnt,
				    const size_t quota) noexcept
{
	if (safe_has_quota_been_exceeded(mnt, quota)) {
		return;
	}

	int f_notify = fanotify_init(
		FAN_CLASS_NOTIF | FAN_REPORT_DFID_NAME_TARGET, O_RDONLY);

	if (f_notify == -1) {
		error_at_line(
			EBADFD, EBADFD, __FILE__, __LINE__,
			"Failed to initialize fanotify(7); exiting with error");
	}

	if (fanotify_mark(f_notify, FAN_MARK_ADD | FAN_MARK_FILESYSTEM,
			  FAN_CREATE | FAN_MODIFY | FAN_MOVED_TO, AT_FDCWD,
			  mnt) == -1) {
		error_at_line(errno, errno, __FILE__, __LINE__,
			      EXITING_WITH_ERRIR);
	}

	const size_t buf_size =
		max(static_cast<size_t>(4096 /
					sizeof(struct fanotify_event_metadata)),
		    static_cast<size_t>(1));
	struct fanotify_event_metadata events[buf_size];
	while (!safe_has_quota_been_exceeded(mnt, quota)) {
		int len_read = read(f_notify, events, sizeof(events));
		if (len_read == -1) {
			if (errno == EAGAIN) {
				errno = 0;
				continue;
			} else {
				error_at_line(errno, errno, __FILE__, __LINE__,
					      EXITING_WITH_ERRIR);
				exit(errno);
			}
		}
	}

	close(f_notify);
}

int main(int argc, char **argv)
{
	const char *mnt = argv[1];
	const size_t quota = atoll(argv[2]);

	monitor_storage_loop(mnt, quota);

	return 0;
}
