EXEC = mnt_monitor
SRC = monitor_mnt_for_freespace.cpp
CXX_ARGS = --std=c++23 -static -static-libgcc -static-libstdc++ -O3 

$(EXEC): $(SRC)
	$(CXX) $(CXX_ARGS)  $^ -o $@

clean:
	rm -f $(EXEC)

